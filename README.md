## Godot 2D - Autonomous Agents Behaviour - Leader Following

> A simple implementation of the steering behaviour "Leader Following" described by [Craig W. Reynolds] in its document [Steering Behaviors For Autonomous Characters].

[Craig W. Reynolds]: <https://www.red3d.com/cwr/>
[Steering Behaviors For Autonomous Characters]: <https://www.red3d.com/cwr/steer/gdc99/>

... todo ...

For each agent, the UI displays few vectors:

- its current velocity

The program run on the [Godot] [1] 3.0 game engine, and is made with [GDScript] language.

## Table of Content

- [Install](#Install)
- [Usage](#Usage)
- [License](#License)

## Install

### Dependencies

Download [Godot] [2] 3.0 game engine.

### Open the project

Download the source code and import the `project.godot` file from Godot 3.0.

## Usage

... todo ...

## License

[MIT](LICENSE.txt) (c) Gresille&Siffle

[1]: <https://godotengine.org/> "Godot game engine site"
[2]: <https://godotengine.org/download> "Godot game engine download page"
[GDScript]: <http://docs.godotengine.org/en/3.0/getting_started/scripting/gdscript/gdscript_basics.html>
