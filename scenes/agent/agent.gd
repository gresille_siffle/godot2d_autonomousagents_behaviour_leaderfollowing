# Steering behaviour
# Follow leader

extends KinematicBody2D

export (int) var mass = 4
export (int) var max_force = 10
export (int) var max_speed = 420

var velocity = Vector2()
var acceleration = Vector2()

var steering_force = Vector2()
var desired_velocity = Vector2()

var slowing_distance = 128

var cohesion_distance = 64
var alignment_distance = 64
var separation_distance = 32

var cohesion_factor = 1.0
var alignment_factor = 0.8
var separation_factor = 1.4

var ahead_leader = 64
var behind_leader = 64
var evasion_distance = 128

var follow_factor = 0.8
var evasion_factor = 2.4

var screen_size = Vector2()

func _ready():
    screen_size = get_viewport_rect().size
    $VisibilityNotifier2D.connect('screen_exited', self, '_teleport')

func seek(target_position):
    # var desired_velocity = (target_position - position).normalized() * max_speed
    # var steering_force = (desired_velocity - velocity).clamped(max_force)
    return ((target_position - position).normalized() * max_speed - velocity).clamped(max_force)

func flee(target_position):
    return ((position - target_position).normalized() * max_speed - velocity).clamped(max_force)

func seek_with_mass(target_position):
    var desired_velocity = (target_position - position).normalized() * max_speed
    var steering_force = (desired_velocity - velocity) / mass
    return steering_force

func seek_and_arrive(target_position):
    var desired_velocity = (target_position - position)
    var target_distance = desired_velocity.length()

    desired_velocity = desired_velocity.normalized() * max_speed
    desired_velocity = approach_target(slowing_distance, target_distance, desired_velocity)

    var steering_force = (desired_velocity - velocity).clamped(max_force)
    return steering_force

func approach_target(slowing_distance, distance_to_target, velocity):
    if distance_to_target < slowing_distance:
        velocity *= (distance_to_target / slowing_distance)
    return velocity

func pursue(target):
    # var distance = self.position.distance_to(target.position)
    # var anticipation = distance / self.max_speed
    # var future_target_position = target.position + target.velocity * anticipation
    return seek(target.position + target.velocity * (position.distance_to(target.position) / max_speed))

func evade(target):
    return flee(target.position + target.velocity * (position.distance_to(target.position) / max_speed))

func cohesion(agents):
    var agent_nbr = 0
    var sum_of_position = Vector2()

    for agent_ref in agents:
        var agent = agent_ref.get_ref()
        if agent:
            var distance = position.distance_to(agent.position)
            if distance > 0 and distance <= cohesion_distance:
                sum_of_position += agent.position
                agent_nbr += 1

    if agent_nbr:
        # this position could be displayed
        # sum_of_position /= agent_nbr
        return seek(sum_of_position / agent_nbr)
    return Vector2()

func alignment(agents):
    var agent_nbr = 0
    var sum_of_velocity = Vector2()

    for agent_ref in agents:
        var agent = agent_ref.get_ref()
        if agent:
            var distance = position.distance_to(agent.position)
            if distance > 0 and distance <= alignment_distance:
                sum_of_velocity += agent.velocity
                agent_nbr += 1

    if agent_nbr:
        sum_of_velocity /= agent_nbr
        # var desired_velocity = sum_of_velocity.normalized() * max_speed
        return (sum_of_velocity.normalized() * max_speed - velocity).clamped(max_force)

    return Vector2()

func separation(agents):
    var agent_nbr = 0
    var separation_force = Vector2()

    for agent_ref in agents:
        var agent = agent_ref.get_ref()
        if agent:
            var distance = position.distance_to(agent.position)
            if distance > 0 and distance <= separation_distance:
                # var diff = (position - agent.position).normalized() / distance
                agent_nbr += 1
                separation_force += (position - agent.position).normalized() / distance

    if agent_nbr:
        # var desired_velocity = (separation_force / agent_nbr).normalized() * max_speed
        return ((separation_force / agent_nbr).normalized() * max_speed - velocity).clamped(max_force)
    else:
        return Vector2()

func flock(agents):
    apply_force(cohesion(agents) * cohesion_factor)
    apply_force(alignment(agents) * alignment_factor)
    apply_force(separation(agents) * separation_factor)

func follow_the_leader(leader, agents):
    apply_force(seek_and_arrive(leader.position + leader.velocity.normalized() * behind_leader * -1) * follow_factor)
    apply_force(separation(agents) * separation_factor)
    var distance = position.distance_to(leader.position + leader.velocity.normalized() * ahead_leader)
    if distance > 0 and distance <= evasion_distance:
        apply_force(evade(leader) * evasion_factor)

func follow_the_mouse():
    apply_force(seek_and_arrive(_get_mouse_position()))

func move():
    velocity += acceleration
    velocity = velocity.clamped(max_speed)
    rotation = velocity.angle() + PI / 2
    move_and_slide(velocity)
    acceleration = Vector2()

func apply_force(force):
    acceleration += force

func _get_mouse_position():
    return get_viewport().get_mouse_position()

func _teleport():
    if position.x > screen_size.x:
        position.x = 0
    elif position.x < 0:
        position.x = screen_size.x

    if position.y > screen_size.y:
        position.y = 0
    elif position.y < 0:
        position.y = screen_size.y
