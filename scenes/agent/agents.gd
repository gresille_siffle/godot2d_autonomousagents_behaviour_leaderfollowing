extends Node

export (PackedScene) var agent_scene

var grid = null
var grid_must_be_rebuilt = false

var leader = null

func _ready():
    get_tree().get_root().connect('size_changed', self, '_on_screensize_changed')

    grid = get_node('/root/Main/Grid')
    for i in range(64):
        var agent = _new_agent(false)
        grid.add(agent)
        add_child(agent)

    leader = weakref(get_children()[0])

func _physics_process(delta):
    # 1. clear the grid, or rebuild it if the screen size has changed
    if grid_must_be_rebuilt:
        grid_must_be_rebuilt = false
        grid.rebuild()
    else:
        grid.clear()

    # 2. fill the grid with the new agents according to their position
    grid.fill(get_children())

    # 3. move agents one by one, interacting only with close neighbors
    for agent in get_children():
        if agent == leader.get_ref():
            agent.follow_the_mouse()
        else:
            var x = int(agent.position.x / grid.resolution)
            var y = int(agent.position.y / grid.resolution)
            agent.follow_the_leader(leader.get_ref(), grid.neighborhood(x, y))
        agent.move()

func set_population_to(number):
    var population_size = get_child_count()
    if population_size < number:
        self._add_several_agents(number - population_size)
    elif population_size > number:
        self._reduce_population_to(number)
    else:
        pass

func _new_agent(use_ui_values):
    randomize()
    var agent = agent_scene.instance()
    var randxy = Vector2(rand_range(-1, 1), rand_range(-1, 1))
    agent.position = Vector2(self.get_viewport_rect().size.x / 2, self.get_viewport_rect().size.y / 2) + randxy
    if use_ui_values:
        agent.max_force = get_node('/root/Main/Interface/LeftSliders/Force/HSlider').value
        agent.max_speed = get_node('/root/Main/Interface/LeftSliders/Speed/HSlider').value
        agent.separation_distance = get_node('/root/Main/Interface/RightSliders/Separation/HSlider').value
    else:
        agent.max_force = 16
        agent.max_speed = 512
        agent.separation_distance = 32
    return agent

func _add_several_agents(number):
    for i in range(number):
        var agent = _new_agent(true)
        add_child(agent)

        if i == 0 and not leader.get_ref():
            leader = weakref(agent)

func _reduce_population_to(number_to_keep):
    var i = 0
    for agent in get_children():
        i += 1
        if i > number_to_keep:
            agent.queue_free()

func _on_screensize_changed():
    grid_must_be_rebuilt = true

    for agent in get_children():
        agent.screen_size = get_viewport_rect().size
