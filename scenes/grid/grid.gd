# Autonomous Agents Behaviour
# Dynamic bin-lattice spacial subdivision

extends Node2D

var row = 0
var col = 0
var grid = []

var resolution = 100

func _ready():
    """Create a two-dimensional array to act as a simple spacial database."""
    rebuild()

func add(agent):
    """Add a single agent to the grid."""
    var x = clamp(int(agent.position.x / resolution), 0, row - 1)
    var y = clamp(int(agent.position.y / resolution), 0, col - 1)
    grid[x][y].append(weakref(agent))

func fill(agents):
    """Fill the grid with several agents."""
    for agent in agents:
        add(agent)

func neighborhood(x, y):
    """Locality query. Return the 8 bins adjacent to the (x, y) bin, and itself."""
    x = clamp(x, 0, row - 1)
    y = clamp(y, 0, col - 1)
    var neighborhood_ = grid[x][y]

    if x + 1 < grid.size():
        neighborhood_ += grid[x + 1][y]
        if y + 1 < grid[x].size():
            neighborhood_ += grid[x + 1][y + 1]
        if y - 1 >= 0:
            neighborhood_ += grid[x + 1][y - 1]

    if x - 1 >= 0:
        neighborhood_ += grid[x - 1][y]
        if y + 1 < grid[x].size():
            neighborhood_ += grid[x - 1][y + 1]
        if y - 1 >= 0:
            neighborhood_ += grid[x - 1][y - 1]

    if y + 1 < grid[x].size():
        neighborhood_ += grid[x][y + 1]
    if y - 1 >= 0:
        neighborhood_ += grid[x][y - 1]

    return neighborhood_

func clear():
    """Clear the grid."""
    for x in range(row):
        for y in range(col):
            grid[x][y].clear()

func rebuild():
    """Rebuild the grid according to the current screen size."""
    grid = []
    var screen_size = get_viewport_rect().size
    row = int(screen_size.x / resolution)
    col = int(screen_size.y / resolution)
    for x in range(row):
        grid.append([])
        for y in range (col):
            grid[x].append([])
