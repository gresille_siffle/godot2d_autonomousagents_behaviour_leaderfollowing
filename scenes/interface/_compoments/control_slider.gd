extends Control

export var text = ''
export var value = 0
export var max_value_ = 128

func _ready():
    $Name.text = self.text
    $Value.text = str(self.value)

    $HSlider.max_value = self.max_value_
    $HSlider.value = self.value
    $HSlider.connect('value_changed', self, '_on_HSlider_value_changed')

func _on_HSlider_value_changed(value):
    $Value.text = str(value)
