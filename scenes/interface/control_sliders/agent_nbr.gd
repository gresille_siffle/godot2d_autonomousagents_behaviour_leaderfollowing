extends VSlider

func _ready():
    self.connect('value_changed', self, '_on_value_changed')

func _on_value_changed(value):
    get_node('/root/Main/Agents').set_population_to(value)
