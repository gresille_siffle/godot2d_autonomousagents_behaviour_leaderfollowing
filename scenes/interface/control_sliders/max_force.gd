extends VSlider

func _ready():
    self.connect('value_changed', self, '_on_value_changed')

func _on_value_changed(value):
    for agent in get_node('/root/Main/Agents').get_children():
        agent.max_force = value
