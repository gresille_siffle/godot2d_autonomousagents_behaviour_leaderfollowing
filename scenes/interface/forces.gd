extends Node2D

var display_forces = true
var display_targets = true

var scale_ = 0.5

func _physics_process(delta):
    update()

func _draw():
    for agent in get_node('/root/Main/Agents').get_children():
        if self.display_forces:
            self.draw_desired_velocity(agent)
            self.draw_velocity(agent)
            self.draw_steering_force(agent)

func draw_velocity(agent):
    draw_line(agent.position, agent.position + agent.velocity * self.scale_, Color(255, 0, 0))

func draw_desired_velocity(agent):
    draw_line(agent.position, agent.position + agent.desired_velocity * self.scale_, Color(255, 255, 0))

func draw_steering_force(agent):
    draw_line(agent.position, agent.position + agent.steering_force * (1 / self.scale_), Color('#22b14c'), 2.0)

func draw_mouse_position():
    draw_circle(get_viewport().get_mouse_position(), 12.0, Color('#22b14c'))
