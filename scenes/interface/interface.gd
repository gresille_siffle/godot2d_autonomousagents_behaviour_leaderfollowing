extends Control

func _ready():
    $LeftSliders/Force/HSlider.connect('value_changed', self, '_on_Force_HSlider_value_changed')
    $LeftSliders/Speed/HSlider.connect('value_changed', self, '_on_Speed_HSlider_value_changed')
    $LeftSliders/Agents/HSlider.connect('value_changed', self, '_on_Agents_HSlider_value_changed')
    $RightSliders/Cohesion/HSlider.connect('value_changed', self, '_on_Cohesion_HSlider_value_changed')
    $RightSliders/Alignment/HSlider.connect('value_changed', self, '_on_Alignment_HSlider_value_changed')
    $RightSliders/Separation/HSlider.connect('value_changed', self, '_on_Separation_HSlider_value_changed')
    self.toggle_control('Forces')

func _input(event):
    if event.is_action_pressed('ui_toggle_pause'):
        get_tree().paused = not get_tree().paused

    if event.is_action_pressed('ui_toggle_ui'):
        self.toggle_control('Ui')
    if event.is_action_pressed('ui_toggle_all'):
        self.toggle_control('All')
    if event.is_action_pressed('ui_toggle_forces'):
        self.toggle_control('Forces')
    if event.is_action_pressed('ui_toggle_targets'):
        self.toggle_control('Targets')

func toggle_ui():
    $Controls.visible = not $Controls.visible
    $LeftSliders.visible = not $LeftSliders.visible
    $RightSliders.visible = not $RightSliders.visible

func hide_ui():
    $Controls.visible = false
    $LeftSliders.visible = false
    $RightSliders.visible = false

func show_ui():
    $Controls.visible = true
    $LeftSliders.visible = true
    $RightSliders.visible = true

func toggle_control(control):
    match control:
        'Ui':
            self.toggle_ui()
            var node = $Controls/Ui
            node.toggle()
        'All':
            var node = $Controls/All
            node.toggle()
            if node.is_pressed:
                self.press_control('Ui')
                self.press_control('Forces')
                self.press_control('Targets')
                self.hide_ui()
            else:
                self.release_control('Ui')
                self.release_control('Forces')
                self.release_control('Targets')
                self.show_ui()
        'Forces':
            $Forces.display_forces = not $Forces.display_forces
            var node = $Controls/Forces
            node.toggle()
        'Targets':
            $Forces.display_targets = not $Forces.display_targets
            var node = $Controls/Targets
            node.toggle()

func press_control(control):
      match control:
        'Ui':
            $Controls.visible = false
            var node = $Controls/Ui
            node.press()
        'Forces':
            $Forces.display_forces = false
            var node = $Controls/Forces
            node.press()
        'Targets':
            $Forces.display_targets = false
            var node = $Controls/Targets
            node.press()

func release_control(control):
      match control:
        'Ui':
            $Controls.visible = true
            var node = $Controls/Ui
            node.release()
        'Forces':
            $Forces.display_forces = true
            var node = $Controls/Forces
            node.release()
        'Targets':
            $Forces.display_targets = true
            var node = $Controls/Targets
            node.release()

func _on_Force_HSlider_value_changed(value):
    for agent in get_node('/root/Main/Agents').get_children():
        agent.max_force = value

func _on_Speed_HSlider_value_changed(value):
    for agent in get_node('/root/Main/Agents').get_children():
        agent.max_speed = value

func _on_Agents_HSlider_value_changed(value):
    get_node('/root/Main/Agents').set_population_to(value)

func _on_Cohesion_HSlider_value_changed(value):
    for agent in get_node('/root/Main/Agents').get_children():
        agent.cohesion_distance = value

func _on_Alignment_HSlider_value_changed(value):
    for agent in get_node('/root/Main/Agents').get_children():
        agent.alignment_distance = value

func _on_Separation_HSlider_value_changed(value):
    for agent in get_node('/root/Main/Agents').get_children():
        agent.separation_distance = value
